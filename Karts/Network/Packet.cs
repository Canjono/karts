using System.Text;

namespace Karts.Network
{
    public class Packet
    {
        public byte[] Data { get; set; }

        public void Serialize(string data)
        {
            Data = Encoding.ASCII.GetBytes(data);
        }
    }
}