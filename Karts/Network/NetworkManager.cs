using System.Net;
using System.Net.Sockets;

namespace Karts.Network
{
    public class NetworkManager
    {
        private readonly UdpClient _udpClient;
        private static NetworkManager _instance;

        public static NetworkManager Instance => _instance ?? (_instance = new NetworkManager());

        public NetworkManager()
        {
            _udpClient = new UdpClient();

            var ipEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 55600);
            _udpClient.Connect(ipEndPoint);
        }
        
        public void Send(Packet packet)
        {
            _udpClient.Send(packet.Data, packet.Data.Length);
        }

        public void SendText(string text)
        {
            var packet = new Packet();
            packet.Serialize(text);
            
            Send(packet);
        }

        public void Destroy()
        {
            _udpClient.Close();
            _instance = null;
        }
    }
}