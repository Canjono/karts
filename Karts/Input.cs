using System;

namespace Karts
{
    public static class InputManager
    {
        private static ConsoleKeyInfo _pressedKey;
        
        public static ConsoleKeyInfo PressedKey
        {
            get
            {
                KeyAvailable = false;
                return _pressedKey;
            }
            private set
            {
                _pressedKey = value;
            }
        }
        
        public static bool KeyAvailable { get; set; }

        public static void CheckPressedKey()
        {
            if (!Console.KeyAvailable)
            {
                KeyAvailable = false;
                return;
            }

            ConsoleKeyInfo readKey;

            do
            {
                readKey = Console.ReadKey(true);
            } while (Console.KeyAvailable);


            _pressedKey = readKey;
            KeyAvailable = true;
        }
    }
}