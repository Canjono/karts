using System;
using System.Threading;
using Karts.Network;

namespace Karts
{
    public class Game
    {
        public void Start()
        {
            Screen.Init(68, 32);
            
            var player = new Player(5, 5);
            var ticks = 0;
            var isPaused = false;
            
            while (player.Alive)
            {
                InputManager.CheckPressedKey();
                
                if (!isPaused)
                {
                    player.Update();
                    ticks++;
                }
                
                if (InputManager.KeyAvailable)
                {
                    switch (InputManager.PressedKey.Key)
                    {
                        case ConsoleKey.Escape:
                            player.Alive = false;
                            break;
                        case ConsoleKey.P:
                            isPaused = !isPaused;
                            NetworkManager.Instance.SendText("Pause button was pressed");
                            break;
                        default:
                            InputManager.KeyAvailable = true;
                            break;
                    }
                }
            
                Thread.Sleep(100);
            }
        }
    }
}