using System;

namespace Karts
{
    public class Screen
    {
        private Screen _instance;
        
        public static int Width { get; set; }
        public static int Height { get; set; }

        public static void Init(int? width, int? height)
        {
            Width = width ?? throw new ArgumentException(nameof(width));
            Height = height ?? throw new ArgumentNullException(nameof(height));
            
            Console.Title = "Snake";
            Console.WindowWidth = Width;
            Console.WindowHeight = Height;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.CursorVisible = false;
        }
    }
}