using System;
using System.Drawing;
using System.Numerics;
using Karts.Network;

namespace Karts
{
    public class Player
    {
        private Point _position;
        private Direction _direction;
        private const int SPEED = 2;
        private readonly Point[] _trail;
        private Point _lastTrail;

        public bool Alive { get; set; }

        public Player(int x, int y)
        {
            _position = new Point(x, y);
            _trail = new Point[2];

            for (var i = 0; i < _trail.Length; i++)
            {
                _trail[i] = new Point(x - (i + 1));
            }

            _direction = Direction.Right;

            Alive = true;
        }

        public void Update()
        {
            Input();
            Move();
            Draw();
        }

        private void Input()
        {
            if (!InputManager.KeyAvailable)
            {
                return;
            }

            switch (InputManager.PressedKey.Key)
            {
                case ConsoleKey.A:
                    _direction = --_direction < 0 ? Direction.EnumLength - 1 : _direction;
                    break;
                case ConsoleKey.D:
                    _direction = (Direction)((int)++_direction % (int)Direction.EnumLength);
                    break;
                default:
                    InputManager.KeyAvailable = true;
                    return;
            }

            NetworkManager.Instance.SendText($"Player pressed: {InputManager.PressedKey.Key}");
        }

        private void Move()
        {
            if (_trail.Length > 0)
            {
                _lastTrail = _trail[0];
                _trail[0] = _trail[1];
                _trail[1] = _position;
            }

            switch (_direction)
            {
                case Direction.Up:
                    if ((_position.Y -= SPEED / 2) < 0) { _position.Y = Screen.Height + _position.Y; }
                    break;
                case Direction.Right:
                    if ((_position.X += SPEED) >= Screen.Width) { _position.X = _position.X % Screen.Width; }
                    break;
                case Direction.Down:
                    if ((_position.Y += SPEED / 2) >= Screen.Height) { _position.Y = _position.Y % Screen.Height; }
                    break;
                case Direction.Left:
                    if ((_position.X -= SPEED) < 0) { _position.X = Screen.Width + _position.X; }
                    break;
                default:
                    break;
            }
        }

        private void Draw()
        {
            var character = ' ';

            switch (_direction)
            {
                case Direction.Up:
                    character = '^';
                    break;
                case Direction.Right:
                    character = '>';
                    break;
                case Direction.Down:
                    character = 'v';
                    break;
                case Direction.Left:
                    character = '<';
                    break;
                default:
                    return;
            }

            Console.SetCursorPosition(_position.X, _position.Y);
            Console.Write(character);


            Console.SetCursorPosition(_lastTrail.X, _lastTrail.Y);
            Console.Write(' ');
        }
    }

    public enum Direction
    {
        Up,
        Right,
        Down,
        Left,
        EnumLength
    }
}