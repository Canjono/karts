﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Listener
{
    class Program
    {
        static void Main(string[] args)
        {
            var done = false;
            var listenPort = 55600;

            using (var listener = new UdpClient(listenPort))
            {
                var listenEndPoint = new IPEndPoint(IPAddress.Any, listenPort);

                while (!done)
                {
                    var receivedData = listener.Receive(ref listenEndPoint);
                    
                    Console.WriteLine("Received broadcast message from client {0}", listenEndPoint.ToString());
                    
                    Console.WriteLine("Decoded data is:");
                    Console.WriteLine(Encoding.ASCII.GetString(receivedData));
                }
            }
        }
    }
}